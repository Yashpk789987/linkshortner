/**
 * This file will create the database function related to the user
 */

import Users from '../models/users';

/**
 * This database function will create the user
 * @param {String} username
 * @param {String} name
 * @param {String} password
 * @param {String} email
 *
 * @example
 * createUser({
 * username: 'palash7563',
 * name: 'palash Gupta',
 * password: 'password1',
 * email: 'palashg@gmail.com',
 * })
 *  .then((e) => { console.log(e);})
 *  .catch((e) => { console.log(e);});
 */
export async function createUser({
 username, name, password, email 
}) {
  try {
    // checking if the user is existing or not
    const existingUser = await getIdByUserName({ username });

    // if user is not found
    if (existingUser.statusCode === 404) {
      const user = new Users({
        username,
        name,
        password,
        email,
      });

      const users = await user.save();

      return {
        statusCode: 201,
        message: 'Success',
        payload: {
          userId: users._id,
        },
      };
    }
    return {
      statusCode: 301,
      message: 'User Already Exist',
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// createUser({
//   username: "palash753",
//   name: "palash Gupta",
//   password: "password1",
//   email: "palashg@gmail.com"
// })
//   .then(e => {
//     console.log(e);
//   })
//   .catch(e => {
//     console.log(e);
//   });

/**
 * This function update the user's name by it's id
 * @async
 * @function updateUserNameByUserName
 *
 * @param {String} name
 * @param {String} email
 * @param {String} id
 * @returns {Promise}
 *
 * @example
 * updateUserByUserName({
 * username: 'palashg7563',
 * name: 'Palash Gupta',
 * email: 'palash@gmail.com',
 * })
 *  .then((e) => console.log(e))
 *  .catch((e) => console.log(e));
 */

export async function updateUserByUserName({ username, name, email }) {
  try {
    const query = Users.findOneAndUpdate(username, { name, email });

    const users = await query.exec();

    if (!users) {
      return {
        statusCode: 404,
        message: 'Not Found',
      };
    }

    return {
      statusCode: 201,
      message: 'Success',
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      message: 'Database Server Error',
    };
  }
}

// updateUserByUserName({
//   username: 'palashg7563',
//   name: 'Palash Gupta',
//   email: 'palash@gmail.com',
// })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * This function is used to get a user by it's userId
 * @function getUserByUserId
 * @param {String} userId
 * @returns {Promise}
 *
 * @example
 * getUserByUserId({ userId: '5afc8be3290642018779bb59' })
 *  .then((e) => console.log(e))
 *  .catch((e) => console.log(e));
 */
export async function getUserByUserId({ userId }) {
  try {
    const query = Users.findById(userId);

    const users = await query.exec();

    if (!users) {
      return {
        statusCode: 404,
        message: 'Not Found',
      };
    }
    return {
      statusCode: 200,
      message: 'Success',
      payload: {
        id: users._id,
        username: users.username,
        email: users.email,
        name: users.name,
      },
    };
  } catch (error) {
    console.log(error);
    return {
      statusCode: 500,
      message: 'Database Server Error',
    };
  }
}
// getUserByUserId({ userId: '5afc8be3290642018779bb59' })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * This function is used to return the id
 * @async
 * @function getIdByUserName
 * @param {String} id
 * @returns {Promise}
 *
 * @example
 * getIdByUserName({ username: 'palashg7563' })
 * .then((e) => console.log(e))
 * .catch((e) => console.log(e));
 */

export async function getIdByUserName({ username }) {
  try {
    const query = Users.findOne({ username });

    const users = await query.exec();

    if (!users) {
      return {
        statusCode: 404,
        message: 'Not Found',
      };
    }

    return {
      statusCode: 200,
      message: 'Success',
      payload: {
        id: users._id,
        username: users.username,
        email: users.email,
        name: users.name,
      },
    };
  } catch (error) {
    console.log(error);
    return {
      statusCode: 500,
      message: 'Database Server Error',
    };
  }
}
// getIdByUserName({ username: 'palashg7563' })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * This function is used to getall the user's
 * @async
 * @function getAllUser
 *
 * @returns {Promise}
 * @example
 */
export async function getAllUser() {
  try {
    const query = Users.find({});

    const users = await query.exec();
    return {
      statusCode: 200,
      message: 'Sucess',
      payload: {
        users,
      },
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      message: 'Datbase Server Error',
    };
  }
}

// getAllUser()
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * This database function is used to delete the user by it's username
 *
 * @async
 * @function deleteByUsername
 * @param {String} username
 *
 * @returns {Promise}
 * @example
 * deleteByUsername({ username: 'palashg7563' })
 *  .then((e) => console.log(e))
 *  .catch((e) => console.log(e));
 *
 */
export async function deleteByUsername({ username }) {
  try {
    const query = Users.findOneAndRemove({ username });

    const users = await query.exec();

    if (!users) {
      return {
        statusCode: 404,
        message: 'Not Found',
      };
    }

    return {
      statusCode: 200,
      message: 'Success',
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      message: 'Database Server Error',
    };
  }
}
// deleteByUsername({ username: 'palashg7563' })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));
