import { getAllUser } from '../../database/mongodb/functions/users';

export default {
  description: "This API will get all the user's",
  notes: 'This is for user AUTH only',
  tags: ['api', 'user'],
  async handler(request, h) {
    try {
      const res = await getAllUser();

      return h.response(res);
    } catch (error) {
      console.error(error);
      return {
        statusCode: 500,
        error: 'Server Error',
      };
    }
  },
};
