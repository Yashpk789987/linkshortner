import createUser from './create';
import deleteUser from './delete';
import updateUser from './update';
import getUser from './get';
import getAllUser from './getAll';

const routes = [];

routes.push({
  method: 'GET',
  path: '/users/{id}',
  config: getUser,
});

routes.push({
  method: 'POST',
  path: '/users',
  config: createUser,
});

routes.push({
  method: 'GET',
  path: '/users',
  config: getAllUser,
});

routes.push({
  method: 'DELETE',
  path: '/user',
  config: deleteUser,
});

routes.push({
  method: 'PUT',
  path: '/users',
  config: updateUser,
});

export default routes;
