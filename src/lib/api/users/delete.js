import joi from 'joi';

import { deleteByUsername } from '../../database/mongodb/functions/users';

export default {
  description: "This API is used to delete the user by it's username",
  notes: 'In auth user must be the user',
  tags: ['api', 'user'],
  validate: {
    payload: {
      username: joi.string().required(),
    },
  },
  async handler(request, h) {
    try {
      const { username } = request.payload;

      const res = await deleteByUsername({ username });

      return h.response(res);
    } catch (error) {
      console.error(error);
      return {
        stausCode: 500,
        error: 'Server Error',
      };
    }
  },
};
