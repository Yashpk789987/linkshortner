import create from './create';
import get from './get';
import getAll from './getAll';
import deleteLink from './delete';

const routes = [];

routes.push({
  method: 'POST',
  path: '/{userId}/link/',
  config: create,
});

routes.push({
  method: 'GET',
  path: '/{userId}/link/{linkId}',
  config: get,
});

routes.push({
  method: 'DELETE',
  path: '/{userId}/link/{linkId}',
  config: deleteLink,
});

routes.push({
  method: 'GET',
  path: '/{userId}/links',
  config: getAll,
});

export default routes;
