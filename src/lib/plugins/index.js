import good from 'good';
import bell from 'bell';
import vision from 'vision';
import inert from 'inert';
import hapiSwagger from 'hapi-swagger';
import jwt2 from 'hapi-auth-jwt2';

import routes from '../api/index';
import authapi from '../auth/index';

const plugin = [];

plugin.push({
  plugin: good,
  options: {
    ops: {
      interval: 1000,
    },
    reporters: {
      myConsoleReporter: [
        {
          module: 'good-squeeze',
          name: 'Squeeze',
          args: [{ log: '*', response: '*' }],
        },
        {
          module: 'good-console',
        },
        'stdout',
      ],
      myFileReporter: [
        {
          module: 'good-squeeze',
          name: 'Squeeze',
          args: [{ ops: '*' }],
        },
        {
          module: 'good-squeeze',
          name: 'SafeJson',
        },
        {
          module: 'good-file',
          args: ['./test/fixtures/awesome_log'],
        },
      ],
      myHTTPReporter: [
        {
          module: 'good-squeeze',
          name: 'Squeeze',
          args: [{ error: '*' }],
        },
        {
          module: 'good-http',
          args: [
            'http://prod.logs:3000',
            {
              wreck: {
                headers: { 'x-api-key': 12345 },
              },
            },
          ],
        },
      ],
    },
  },
});

const shortnerApi = {
  register: async (server, options) => {
    server.route(routes);
  },
  name: 'shortnerApi',
  version: '1.0.0',
};

const AuthApi = {
  register: authapi,
  name: 'AuthAPi',
  version: '1.0.0',
};

plugin.push({
  plugin: jwt2,
  option: {},
});

plugin.push({
  plugin: AuthApi,
  options: {},
});

plugin.push({
  plugin: shortnerApi,
  options: {},
});

plugin.push({
  plugin: bell,
  options: {},
});

plugin.push({
  plugin: vision,
  options: {},
});

plugin.push({
  plugin: inert,
  option: {},
});

plugin.push({
  plugin: hapiSwagger,
  option: {
    info: {
      title: 'Link Shortner API',
      version: '1.0.0',
    },
    contact: {
      name: 'Palash Gupta',
      email: 'palashg7563@gmail.com',
    },
    grouping: 'tags',
    tags: [
      {
        name: 'api',
        description: 'APIs',
      },
      {
        name: 'user',
        description: 'API of the User',
      },
      {
        name: 'link',
        description: 'API for the link of the user',
      },
    ],
  },
});

module.exports = plugin;
